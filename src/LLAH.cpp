/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   LLAH.cpp
@brief  the core implementation of TRDM.
@author Yuji Oyamada and Hideaki Uchiyama
*/

#include "LLAH.h"
//#include "Timer.h"
#include "SpectralMethod.h"

#include <cmath>
#include <algorithm>
#include <functional>

#ifdef _WIN32
#include <direct.h>
#else
//#include <stdio.h>
//#include <sys/stat.h>
#endif


const std::string llahname = "llah";
const std::string hashname = "hash";
const std::string markername = "marker";
const std::string pairname = "pair";

/*!
@brief		constructor
@author		H.Uchiyama
*/
CLLAH::CLLAH()
{
}

/*!
@brief		destructor
@author		H.Uchiyama
*/
CLLAH::~CLLAH()
{
}

bool CLLAH::load_config(const std::string filename)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        return false;
    }


    m_param.Init(int(fs["n"]),
                 int(fs["m"]),
                 4,
                 int(fs["qbit"]));

    fs.release();

    return true;
}

bool CLLAH::init(const std::string filename)
{
    load_config(filename);
    m_bmymethod = true;
    m_bhungarian = false;
    m_bpairwise = false;
    m_bbackside = false;
    m_bpnp = false;
    m_markersize = 2000;

    return true;
}

bool CLLAH::init(const sLLAHParam param)
{
    m_param.Init(param.n,
                 param.m,
                 param.k,
                 param.qbit);
    m_bmymethod = true;
    m_bhungarian = false;
    m_bpairwise = false;
    m_bbackside = false;
    m_bpnp = false;
    m_markersize = 2000;

    return true;
}

bool CLLAH::build(const std::vector< std::vector< cv::Point2f > >& markers)
{
    std::vector< std::vector<int> > &wnCm = m_bmymethod ? m_param.wnCm : m_param.wnCmall;
    std::vector< std::vector<int> > &wmCk = m_param.wmCk;

    // set all points
    m_wrefpt = markers;
    int num_markers = markers.size();

    // compute all features
    std::vector<float> vratio;
    for (int i = 0; i < num_markers; ++i){

        // get points
        std::vector<cv::Point2f> &vpt = m_wrefpt[i];
        std::vector<float> vraw;
        std::vector< std::vector<int> > wneighbor;
        computeRawDescriptors(wnCm, wmCk, vpt, vraw, wneighbor);

        // for each point
        for (int j = 0, jend = int(vpt.size()); j < jend; ++j){
            for (int k = 0, kend = int(wnCm.size()); k < kend; ++k){

                int size = int(wmCk.size());
                int pos = (k * size) + j * (kend * size);
                for (int l = pos, lend = pos + size; l < lend; ++l){
                    vratio.push_back(vraw[l]);
                }
            }
        }
    }

    // compute quantization level
    std::sort(vratio.begin(), vratio.end());
    m_param.ComputeQuantization(vratio);

    // compute hash table
#ifdef VTABLE
    m_table.resize(m_param.hashsize);
#endif

    computeHashtable();

    // initialize homographies
    m_H = std::vector<cv::Matx33d>(num_markers);
    m_found = std::vector<int>(num_markers, -1);

    return true;
}

void CLLAH::find(
    const std::vector< cv::Point2f >& vpt
)
{
    const std::vector< std::vector<int> > &wnCm = m_param.wnCm;
    const std::vector< std::vector<int> > &wmCk = m_param.wmCk;

    // compute descriptors
    std::vector< std::vector<int> > wdesc;
    std::vector< std::vector<int> > wneighbor;
    computeDescriptors(wnCm, wmCk, vpt, wdesc, wneighbor);

    // matching
    int nummarkers = int(m_wrefpt.size());		// num of markers
    std::vector< std::vector< cv::Point2f > > wref(nummarkers), wquery(nummarkers);
    m_num_queries = int(wdesc.size());
    m_matchings.clear();
    for (int i = 0, iend = m_num_queries; i < iend; ++i){
        std::vector<int> &vdesc = wdesc[i];

        unsigned id;
        if (searchID(vdesc, id)){
            unsigned markerid = getMarkerID(id);
            unsigned ptid = getPtID(id);
            wquery[markerid].push_back(vpt[i]);
            wref[markerid].push_back(m_wrefpt[markerid][ptid]);
        }
    }

    // compute pose
    std::vector<uchar> inliers;
    m_H.clear();
    for(int i = 0; i < nummarkers; ++i){
        if(wref[i].size() > 4){
            cv::findHomography(wref[i], wquery[i], cv::RANSAC, 4.0, inliers, 20);
            for(size_t j = 0; j < inliers.size(); ++j){
                if(inliers[j] != 0){
                    m_matchings.push_back(sMatching(wquery[i][j], wref[i][j], i, j));
                }
            }
        }
    }
}

void CLLAH::find(
    const std::vector< cv::Point2f >& vpt,
    std::vector<sRetrieval>& matches
)
{
    const std::vector< std::vector<int> > &wnCm = m_param.wnCm;
    const std::vector< std::vector<int> > &wmCk = m_param.wmCk;

    // compute descriptors
    std::vector< std::vector<int> > wdesc;
    std::vector< std::vector<int> > wneighbor;
    computeDescriptors(wnCm, wmCk, vpt, wdesc, wneighbor);

    // matching
    int num_markers = int(m_wrefpt.size());		// num of markers
    std::vector< std::vector< cv::Point2f > > wref(num_markers), wquery(num_markers);
    matches.clear();
    m_num_queries = int(wdesc.size());
    m_matchings.clear();
    for (int i = 0, iend = m_num_queries; i < iend; ++i){
        std::vector<int> &vdesc = wdesc[i];

        unsigned id;
        if (searchID(vdesc, id)){		// find ID

            unsigned markerid = getMarkerID(id);
            unsigned ptid = getPtID(id);
            matches.push_back(sRetrieval(i, markerid, ptid));
            wquery[markerid].push_back(vpt[i]);
            wref[markerid].push_back(m_wrefpt[markerid][ptid]);

        }
    }

    // compute pose
    std::vector<uchar> inliers;
    cv::Mat H;
    for(int i = 0; i < num_markers; ++i){
        m_found[i] = -1;
        m_H[i] = cv::Matx33d();
        if(wref[i].size() > 4){
            H = cv::findHomography(wref[i], wquery[i], cv::RANSAC, 4.0, inliers, 20);
            if(std::accumulate(inliers.begin(), inliers.end(), 0) > 4){
                m_found[i] = 1;
                m_H[i] = H;
            }
            for(size_t j = 0; j < inliers.size(); ++j){
                if(inliers[j] != 0){
                    m_matchings.push_back(sMatching(wquery[i][j], wref[i][j], i, j));
                }
            }
        }
    }
}

void CLLAH::draw_points(cv::Mat& img){
    cv::Scalar color = cv::Scalar(0, 0, 255);
    for(size_t i = 0; i < m_matchings.size(); ++i){
        cv::circle(img, m_matchings[i].query, 5, color, -1);
    }

}

void CLLAH::draw_markers(cv::Mat& img,
                         const std::vector<cv::Point2f>& pts){
    std::vector<cv::Point2f> pts_img{pts.size()};
    for(size_t i = 0; i < m_found.size(); ++i)
    {
        if(m_found[i]){
            cv::perspectiveTransform(pts, pts_img, m_H[i]);
            cv::Point2f pt_center(0.0f, 0.0f);
            for(size_t j = 0; j < pts.size(); ++j)
            {
                cv::line(img, pts_img[j], pts_img[(j+1)%4], cv::Scalar(255, 0, 0), 3);
                pt_center += pts_img[j] / 4.0f;
            }
            cv::putText(img, std::to_string(i), pt_center, 1, 3, cv::Scalar(0, 0, 255));
        }
    }
}

bool CLLAH::save_matches(const std::string filename)
{
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);

    fs << "num_points" << m_num_queries;
    fs << "num_markers" << int(m_wrefpt.size());
    fs << "matches" << "{";
    for(auto match = m_matchings.cbegin(); match != m_matchings.cend(); ++match)
    {
        int id = int(std::distance(m_matchings.cbegin(), match));
        fs << "match" + std::to_string(id) << "{";

        fs << "query_x" << match->query.x;
        fs << "query_y" << match->query.y;
        fs << "reference_x" << match->reference.x;
        fs << "reference_y" << match->reference.y;
        fs << "id_marker" << match->id_marker;
        fs << "id_point" << match->id_point;

        fs << "}";
    }
    fs << "}";
    fs.release();

    return true;
}

/*!
@brief		search ID from descriptors
@param[in]	vdesc	list of descriptors
@param[out]	vid		list of ID with frequency
@retval		true	found
@retval		false	not found
*/
void CLLAH::searchID(const std::vector<int> &vdesc, std::vector<sortelem<int>> &vid) const
{
	std::vector<unsigned> vallid;
	vallid.reserve(vdesc.size() * 128);	// 128 is meaningless

	// make histogram
	for (int i = 0, iend = int(vdesc.size()); i < iend; ++i){

		std::vector<unsigned> vid;
		findID(vdesc[i], vid);

		for (int j = 0, jend = int(vid.size()); j < jend; ++j){
			vallid.push_back(vid[j]);
		}
	}

	if (vallid.size() != 0){
		// sort
		std::sort(vallid.begin(), vallid.end());

		vid.reserve(vallid.size());

		// gather IDs
		sortelem<int> elem;
		elem.val = 1;
		for (int i = 1, iend = int(vallid.size()); i < iend; ++i){
			if (vallid[i - 1] == vallid[i]){
				++elem.val;
			}
			else{
				elem.id = vallid[i - 1];
				vid.push_back(elem);
				elem.val = 1;
			}
		}

		elem.id = vallid[int(vallid.size()) - 1];
		vid.push_back(elem);
	}
}

/*!
@brief		search ID from descriptors
@param[in]	vdesc	list of descriptors
@param[out]	id		searched ID
@retval		true	found
@retval		false	not found
*/
bool CLLAH::searchID(const std::vector<int> &vdesc, unsigned &id) const
{
	std::vector<unsigned> vallid;
	vallid.reserve(vdesc.size() * 128);	// 128 is meaningless

	// make histogram
	for (int i = 0, iend = int(vdesc.size()); i < iend; ++i){

		std::vector<unsigned> vid;
		findID(vdesc[i], vid);

		for (int j = 0, jend = int(vid.size()); j < jend; ++j){
			vallid.push_back(vid[j]);
		}
	}

	// search max
	if (vallid.size() == 0){
		return false;
	}
	else{
		std::sort(vallid.begin(), vallid.end());

		id = vallid[0];
		int count = 1;
		int maxcount = 1;
		int samecount = 0;

		for (int i = 1, iend = int(vallid.size()); i < iend; ++i){
			if (vallid[i - 1] == vallid[i]){
				++count;
			}
			else{
				if (maxcount < count){
					id = vallid[i - 1];
					maxcount = count;
					samecount = 0;
				}
				else if (maxcount == count){
					++samecount;
				}
				count = 0;
			}
		}

		if (maxcount < count){
			id = vallid[int(vallid.size()) - 1];
		}
		else if (maxcount == count){
			++samecount;
		}

		if (samecount > 0){
			return false;
		}
		else{
			return true;
		}
	}
}

/*!
@brief		find ID from hash table
@param[in]	index	index
@param[in]	vid		list of id
*/
void CLLAH::findID(const int index, std::vector<unsigned> &vid) const
{
#ifdef VTABLE
	vid = m_table[index];
#else
	htable::const_iterator it = m_table.find(index);

	if (it != m_table.end()){
		vid = it->second;
	}
#endif
}

/*!
@brief		compute ID
@param[in]	markerID	marker ID
@param[in]	ptID		point ID
@retval		ID
*/
unsigned CLLAH::computeID(const unsigned short markerID, const unsigned short ptID) const
{
	unsigned ID = markerID;
	ID = ID << BITWIDTH;		// 16 bit shift
	return ID | ptID;
}

/*!
@brief		get marker ID
@param[in]	ID			ID
@retval		marker ID
*/
unsigned CLLAH::getMarkerID(const unsigned ID) const
{
	return ID >> BITWIDTH;
}

/*!
@brief		get marker ID
@param[in]	ID			ID
@retval		point ID
*/
unsigned CLLAH::getPtID(const unsigned ID) const
{
	return 0x0000ffff & ID;
}

/*!
@brief		add index to hash table
@param[in]	index	index
@param[in]	id		id
*/
void CLLAH::addHashtable(const int index, const unsigned &id)
{
#ifdef VTABLE
	std::vector<unsigned> &tmp = m_table[index];
	tmp.push_back(id);

	for (int i = 0, iend = int(tmp.size()) - 1; i < iend; ++i){
		if (id == tmp[i]){
			tmp.pop_back();
			break;
		}
	}

#else
	htable::iterator it = m_table.find(index);

	if (it == m_table.end()){
		std::vector<unsigned> tmp;
		tmp.push_back(id);
		m_table.insert(std::pair<int, std::vector<unsigned>>(index, tmp));
	}
	else{
		std::vector<unsigned> &tmp = it->second;
		tmp.push_back(id);

		for (int i = 0, iend = int(tmp.size()) - 1; i < iend; ++i){
			if (id == tmp[i]){
				tmp.pop_back();
				break;
			}
		}
	}
#endif
}

/*!
@brief		compute pairwise data
@param[in]	wdesc		list of descriptors for each point
@param[in]	wneighbor	list of neighbors for each point
@param[out]	pairwise	pairwise data
*/
void CLLAH::computePairwise(const std::vector< std::vector<int> > &wdesc, const std::vector< std::vector<int> > &wneighbor, CPairwiseMat<int> &pairwise) const
{
	const int numpts = int(wdesc.size());

	pairwise.resize(numpts, numpts);

	// for each point
	for (int i = 0, iend = int(wdesc.size()); i < iend; ++i){
		const std::vector<int> &vdesci = wdesc[i];
		const std::vector<int> &vindex = wneighbor[i];

		// for each neighbor point
		for (int j = 0, jend = int(vindex.size()); j < jend; ++j){

			int indexj = vindex[j];
			const std::vector<int> &vdescj = wdesc[indexj];

			std::vector<int> same;

			std::set_intersection(vdesci.begin(), vdesci.end(), vdescj.begin(), vdescj.end(), std::back_inserter(same));

			if (same.size() != 0){
				pairwise.set(i, indexj, same);
			}
		}
	}
}

/*!
@brief		compute hash table
*/
void CLLAH::computeHashtable(void)
{
	std::vector< std::vector<int> > &wnCm = m_bmymethod ? m_param.wnCm : m_param.wnCmall;
	std::vector< std::vector<int> > &wmCk = m_param.wmCk;

	const int nummarkers = int(m_wrefpt.size());

	m_vpairwise.resize(nummarkers);

	// for each marker
	for (int i = 0; i < nummarkers; ++i){

		// get points
		std::vector<cv::Point2f> vpt = m_wrefpt[i];

		// compute descriptors for each point
		std::vector< std::vector<int> > wdesc;
		std::vector< std::vector<int> > wneighbor;
		computeDescriptors(wnCm, wmCk, vpt, wdesc, wneighbor);

		// compute pairwise data
		if (m_bpairwise){
			computePairwise(wdesc, wneighbor, m_vpairwise[i]);
		}

		// add descriptors to hash table
		for (int j = 0, jend = int(wdesc.size()); j < jend; ++j){

			for (int k = 0, kend = int(wnCm.size()); k < kend; ++k){

				int index = wdesc[j][k];

				unsigned id = computeID((unsigned short)(i), (unsigned short)(j));
				addHashtable(index, id);
			}
		}

		if (m_bbackside){
			std::vector<cv::Point2f> vinvpt = m_wrefpt[i];
			for (int j = 0, jend = int(vinvpt.size()); j < jend; ++j){
				vinvpt[j].x = float(m_markersize) - vinvpt[j].x;
			}

			// compute descriptors for each point
			wdesc.clear();
			computeDescriptors(wnCm, wmCk, vinvpt, wdesc, wneighbor);

			// add descriptors to hash table
			for (int j = 0, jend = int(wdesc.size()); j < jend; ++j){

				for (int k = 0, kend = int(wnCm.size()); k < kend; ++k){

					int index = wdesc[j][k];

					unsigned id = computeID((unsigned short)(i), (unsigned short)(j));
					addHashtable(index, id);
				}
			}
		}
	}
}

/*!
@brief		compute affine invariant from four points
@param[in]	p1	point
@param[in]	p2	point
@param[in]	p3	point
@param[in]	p4	point
@retval		affine invariant
*/
float CLLAH::computeAffineInvariant(const cv::Point2f &p1, const cv::Point2f &p2, const cv::Point2f &p3, const cv::Point2f &p4) const
{
	return computeTriangle(p1, p2, p4) / (computeTriangle(p2, p3, p4) + 0.0000001f);
}

/*!
@brief		compute triangle from three points
@param[in]	p1	point
@param[in]	p2	point
@param[in]	p3	point
@retval		area
*/
float CLLAH::computeTriangle(const cv::Point2f &p1, const cv::Point2f &p2, const cv::Point2f &p3) const
{
	return 0.5f*fabs((p1.x - p3.x)*(p2.y - p3.y) - (p1.y - p3.y)*(p2.x - p3.x));
}

/*!
@brief		sort neighbor points by angle
@param[in]		pt		target point
@param[in]		vpt		list of points
@param[in,out]	windex	list of sorted neighbor points for each point
*/
void CLLAH::sortbyAngle(const cv::Point2f &pt, const std::vector< cv::Point2f > &vpt, std::vector<int> &vindex) const
{
	std::vector<sortelem<float>> vangle(vindex.size());

	// compute angle
	for (int i = 0, iend = int(vindex.size()); i < iend; ++i){
		int id = vindex[i];
		float y = vpt[id].y - pt.y;
		float x = vpt[id].x - pt.x;
		vangle[i].id = id;
		vangle[i].val = cv::fastAtan2(y, x);
	}

	// sort by angle
	std::sort(vangle.begin(), vangle.end());
	for (int i = 0, iend = int(vindex.size()); i < iend; ++i){
		vindex[i] = vangle[i].id;
	}
}

/*!
@brief		sort neighbor points by angle (slower)
@param[in]		pt		target point
@param[in]		vpt		list of points
@param[in,out]	windex	list of sorted neighbor points for each point
*/
void CLLAH::sortbyAngle2(const cv::Point2f &pt, const std::vector< cv::Point2f > &vpt, std::vector<int> &vindex) const
{
	// compute angle
	std::multimap<float, int> vangle;
	for (int i = 0, iend = int(vindex.size()); i < iend; ++i){
		int id = vindex[i];
		float y = vpt[id].y - pt.y;
		float x = vpt[id].x - pt.x;
		vangle.insert(std::pair<float, int>(cv::fastAtan2(y, x), id));
	}

	// sort by angle
	std::multimap<float, int>::iterator it = vangle.begin();
	for (int i = 0, iend = int(vindex.size()); i < iend; ++i, ++it){
		vindex[i] = it->second;
	}
}

/*!
@brief		select neighbor points
@param[in]	num		num of neighbor points
@param[in]	vpt		list of points
@param[out]	windex list of neighbor points for each point
*/
void CLLAH::getNeighbors(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const
{
	// compute distance matrix
	int numvpt = int(vpt.size());

	cv::Mat dist(numvpt, numvpt, CV_64F);

	for (int i = 0; i < numvpt; ++i){
		dist.at<double>(i, i) = 0.0;
		for (int j = i + 1; j < numvpt; ++j){
			dist.at<double>(i, j) = dist.at<double>(j, i) = cv::norm(vpt[i] - vpt[j]);
		}
	}

	// search neighbors
	windex.resize(numvpt);

	std::vector<sortelem<double>> vdist(num + 1);
	for (int i = 0; i < numvpt; ++i){
		const double *d = dist.ptr<double>(i);

		for (int j = 0; j < num + 1; ++j){
			vdist[j].val = d[j];
			vdist[j].id = j;
		}

		std::sort(vdist.begin(), vdist.end(), std::greater<sortelem<double>>());

		for (int j = num + 1; j < numvpt; ++j){
			if (vdist[0].val > d[j]){
				vdist[0].val = d[j];
				vdist[0].id = j;
				sort(vdist.begin(), vdist.end(), std::greater<sortelem<double>>());
			}
		}

		windex[i].resize(num);
		for (int j = 0; j < num; ++j){
			windex[i][j] = vdist[j].id;
		}
	}
}

/*!
@brief		select neighbor points (slower)
@param[in]	num		num of neighbor points
@param[in]	vpt		list of points
@param[out]	windex list of neighbor points for each point
*/
void CLLAH::getNeighbors3(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const
{
	// compute distance matrix
	int numvpt = int(vpt.size());

	cv::Mat dist(numvpt, numvpt, CV_32F);

	for (int i = 0; i < numvpt; ++i){
		dist.at<float>(i, i) = 0.0;
		for (int j = i + 1; j < numvpt; ++j){
			dist.at<float>(i, j) = dist.at<float>(j, i) = float(cv::norm(vpt[i] - vpt[j]));
		}
	}

	// search neighbors
	windex.resize(numvpt);

	for (int i = 0; i < numvpt; ++i){
		const float *d = dist.ptr<float>(i);

		std::multimap<float, int> vneighbor;	// + of neighbors

		for (int j = 0; j < num + 1; ++j){
			vneighbor.insert(std::pair<float, int>(d[j], j));
		}

		for (int j = num + 1; j < numvpt; ++j){
			std::multimap<float, int>::iterator it = vneighbor.end();
			--it;
			if (d[j] < (*it).first){
				vneighbor.erase(it);
				vneighbor.insert(std::pair<float, int>(d[j], j));
			}
		}

		std::multimap<float, int>::iterator it = vneighbor.begin();
		++it;	// nearest point is ingnored because it is same one

		windex[i].resize(num);
		for (int j = 0; j < num; ++j, ++it){
			windex[i][j] = it->second;
		}
	}
}

/*!
@brief		select neighbor points by flann (slower)
@param[in]	num		num of neighbor points
@param[in]	vpt		list of points
@param[out]	windex list of neighbor points for each point
*/
void CLLAH::getNeighbors2(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const
{
	windex.resize(vpt.size());

	cv::Mat data = cv::Mat(vpt).reshape(1);

	// build tree
	cv::flann::Index idx(data, cv::flann::AutotunedIndexParams());
	cv::Mat_<float> query(1, 2);
	std::vector<float> vdist;

	// search neighbors
	for (int i = 0, iend = int(vpt.size()); i < iend; ++i){
		vdist.clear();

		query(0, 0) = vpt[i].x;
		query(0, 1) = vpt[i].y;

		idx.knnSearch(query, windex[i], vdist, num + 1);
		windex[i].erase(windex[i].begin());	// nearest point is ingnored because it is same one
	}
}


/*!
@brief		compute LLAH descriptors
@param[in]	wnCm	list of combinations for LLAH
@param[in]	wmCk	list of combinations for LLAH
@param[in]	vpt		list of points
@param[out]	wdesc	list of descriptors for each point
@param[out]	wneighbor	list of neighbor points for each point
*/
void CLLAH::computeDescriptors(const std::vector< std::vector<int> > &wnCm, const std::vector< std::vector<int> > &wmCk, const std::vector< cv::Point2f > &vpt, std::vector< std::vector< int > > &wdesc, std::vector< std::vector<int> > &wneighbor) const
{
	// compute rawdescriptors for each point
	std::vector<float> vraw;
	computeRawDescriptors(wnCm, wmCk, vpt, vraw, wneighbor);

	// compute descriptor
	int vptsize = int(vpt.size());
	wdesc.resize(vptsize);

	int mCksize = int(wmCk.size());

	for (int i = 0; i < vptsize; ++i){
		wdesc[i].resize(wnCm.size());
		for (int j = 0, jend = int(wnCm.size()); j < jend; ++j){

			unsigned index = 0;

			int pos = (j * mCksize) + i * (jend * mCksize);

			for (int k = pos, kend = pos + mCksize; k < kend; ++k){

				float val = vraw[k];

				index = index << m_param.qbit;
				index += m_param.GetQLevel(val);

				if (index > m_param.hashsize){
					index = index % m_param.hashsize;
				}
			}

			wdesc[i][j] = index;
		}

		std::sort(wdesc[i].begin(), wdesc[i].end());
	}
}

/*!
@brief		compute raw descriptors
@param[in]	wnCm	list of combinations for LLAH
@param[in]	wmCk	list of combinations for LLAH
@param[in]	vpt		list of points
@param[out]	vraw	raw descriptors for each point (1D array for efficient memory use)
@param[out] wneighbor	list of neighbor points for each point
*/
void CLLAH::computeRawDescriptors(const std::vector< std::vector<int> > &wnCm, const std::vector< std::vector<int> > &wmCk, const std::vector< cv::Point2f > &vpt, std::vector< float > &vraw, std::vector< std::vector<int> > &wneighbor) const
{
	// get neighbor points
	getNeighbors(m_param.n, vpt, wneighbor);

	vraw.resize(vpt.size()*wnCm.size()*wmCk.size());
	int vptsize = int(vpt.size());

	std::vector<int> vsortednCm(wnCm[0].size());

	// for each point
	for (int i = 0; i < vptsize; ++i){

		std::vector<int> &vindex = wneighbor[i];	// neighbor points

		sortbyAngle(vpt[i], vpt, vindex);

		for (int j = 0, jend = int(wnCm.size()); j < jend; ++j){
			const std::vector<int> &vnCm = wnCm[j];

			if (m_bmymethod){

				// sort by orientation estimation (find max affine invariant)
				int maxpos = -1;
				float maxval = -1.0f;

				for (int k = 0, kend = int(vnCm.size()); k < kend; ++k){
					const cv::Point2f &p1 = vpt[i];
					const cv::Point2f &p2 = vpt[vindex[vnCm[k]]];
					const cv::Point2f &p3 = vpt[vindex[vnCm[k + 1 < kend ? k + 1 : k + 1 - kend]]];
					const cv::Point2f &p4 = vpt[vindex[vnCm[k + 2 < kend ? k + 2 : k + 2 - kend]]];

					float val = computeAffineInvariant(p1, p2, p3, p4);

					if (val > maxval){
						maxval = val;
						maxpos = k;
					}
				}

				for (int k = 0, kend = int(vnCm.size()); k < kend; ++k, ++maxpos){
					if (maxpos == kend){
						maxpos = 0;
					}
					vsortednCm[k] = vnCm[maxpos];
				}
			}

			int size = int(wmCk.size());
			int pos = (j * size) + i * (jend * size);
			float *val = &vraw[pos];

			for (int k = 0, kend = size; k < kend; ++k, ++val){

				const std::vector<int> &vmCk = wmCk[k];

				const cv::Point2f &p1 = m_bmymethod ? vpt[i] : vpt[vindex[vnCm[vmCk[0]]]];
				const cv::Point2f &p2 = m_bmymethod ? vpt[vindex[vsortednCm[vmCk[0]]]] : vpt[vindex[vnCm[vmCk[1]]]];
				const cv::Point2f &p3 = m_bmymethod ? vpt[vindex[vsortednCm[vmCk[1]]]] : vpt[vindex[vnCm[vmCk[2]]]];
				const cv::Point2f &p4 = m_bmymethod ? vpt[vindex[vsortednCm[vmCk[2]]]] : vpt[vindex[vnCm[vmCk[3]]]];

				*val = computeAffineInvariant(p1, p2, p3, p4);
			}
		}
	}
}
//-------------------------------------------------------------------------------------------


/*!
@brief		get data
@param[in]	_row	row position
@retval		list of pariwise data
*/
template <typename T>
const std::vector< pairelem<T> >& CPairwiseMat<T>::get(const int _row) const
{
	return m_welem[_row];
}

/*!
@brief		set data
@param[in]	_row	row position
@param[in]	_col	col position
@param[in]	data	pariwise data
*/
template <typename T>
void CPairwiseMat<T>::set(const int _row, const int _col, const std::vector<T> &vdata)
{
	pairelem<T> tmp;
	tmp.c = _col, tmp.vdata = vdata;
	m_welem[_row].push_back(tmp);
}


/*!
@brief		set size
@param[in]	_row	num of rows
@param[in]	_col	num of cols
*/
template <typename T>
void CPairwiseMat<T>::resize(const int _row, const int _col)
{
	m_row = _row, m_col = _col;
	m_welem.resize(m_row);
}

/*!
@brief		read data
@param[in]	out		std::ofstream
*/
template <typename T>
bool CPairwiseMat<T>::load(std::ifstream &in)
{
	in >> m_row >> m_col;
	m_welem.resize(m_row);

	for (int i = 0; i < m_row; ++i){

		int size;
		in >> size;

		for (int j = 0; j < size; ++j){
			int c, datasize;

			in >> c >> datasize;

			std::vector<T> data(datasize);

			for (int j = 0; j < datasize; ++j){
				in >> data[j];
			}

			set(i, c, data);
		}
	}

	return true;
}

/*!
@brief		write data
@param[in]	out		std::ofstream
*/
template <typename T>
void CPairwiseMat<T>::save(std::ofstream &out) const
{
	out << m_row << " " << m_col << std::endl;

	for (int i = 0; i < m_row; ++i){

		int size = int(m_welem[i].size());
		out << size << " ";

		for (int j = 0; j < size; ++j){

			const std::vector<T> &vdata = m_welem[i][j].vdata;

			int datasize = int(vdata.size());

			out << m_welem[i][j].c << " ";
			out << datasize << " ";
			for (int k = 0; k < datasize; ++k){
				out << vdata[k] << " ";
			}
		}
		out << std::endl;
	}
}

//-------------------------------------------------------------------------------------------

/*!
@brief		initialize parameters
@param[in]	_n		n
@param[in]	_m		m
@param[in]	_k		k
@param[in]	_qbit	qbit
@retval		true	succeeded
@retval		false	failse
*/
bool sLLAHParam::Init(const int _n, const int _m, const int _k, const int _qbit)
{
	n = _n;
	m = _m;
	k = _k;
	qbit = _qbit;

	if (n == 0 || m == 0 || k == 0 || qbit == 0){
		return false;
	}

	wnCmall = CCombination::GetAllRotatedCombination(n, m);
	wnCm = CCombination::GetCombination(n, m);
	wmCk = CCombination::GetCombination(m, k);

	hashsize = int(pow(2, 8 * sizeof(unsigned) - qbit)) - 1;

	return true;
}

/*!
@brief		set quantization levels
@param[in]	data	for selecting quantization levels
*/
void sLLAHParam::ComputeQuantization(const std::vector<float> &data)
{
	int qsize = int(pow(2, qbit));
	float step = float(data.size()) / float(qsize);

	for (int i = 1, iend = qsize; i < iend; ++i){
		int pos = int(step*float(i));

		// there is no reason why use average of 5 values
		float val = (data[pos - 2] + data[pos - 1] + data[pos] + data[pos + 1] + data[pos + 2]) / 5.0f;
		qlevel.push_back(val);
	}
}

/*!
@brief		return quantization level
@param[in]	val		value
@retval		quantization lavel
*/
int sLLAHParam::GetQLevel(const float val) const
{
	for (int i = 0, iend = int(qlevel.size()); i < iend; ++i){
		if (val < qlevel[i]){
			return i;
		}
		else if (qlevel[iend - 1 - i] < val){
			return iend - i;
		}
	}

	return -1;	// error
}
