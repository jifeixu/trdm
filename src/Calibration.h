/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Calibration.h
@brief  the core implementation for camera calibration.
@author Yuji Oyamada
*/

#include <vector>

#include <opencv2/opencv.hpp>

#include <ceres/ceres.h>
#include <ceres/rotation.h>

#pragma once
namespace calibration{

using points3d = std::vector<cv::Point3f>;
using points2d = std::vector<cv::Point2f>;
using ReturnCostFunction = void *(*)(void *, void*);

struct sData{
    sData();
    ~sData();

    bool set(const cv::Size _size_image,
             const std::vector<points3d>& _objects,
             const std::vector<points2d>& _images);
    cv::Size size_image;
    std::vector<points3d> objects;
    std::vector<points2d> images;

    size_t num_images;
    std::vector<size_t> num_points;
};

struct sParameter{
    sParameter();
    ~sParameter();

    void printK() const ;
    void printDelta() const ;
    void printIntrinsics() const {printK(); printDelta();}
    void printExtrinsics() const ;
    void print() const ;

    bool save(const std::string filename) const ;
    bool load(const std::string filename);

    cv::Mat K;
    cv::Mat delta;
    std::vector<cv::Mat> rvecs;
    std::vector<cv::Mat> tvecs;
};

struct sCostFocalSameDeltaP2 {
    sCostFocalSameDeltaP2(const cv::Point3f _point,
                          const cv::Point2f _observation);

    template <typename T>
    bool operator()(const T* const param_K,
                    const T* const delta,
                    const T* const rvecs,
                    const T* const tvecs,
                    T* residuals) const ;

    static ceres::CostFunction* Create(const cv::Point3f _point,
                                       const cv::Point2f _observation) {
        return (new ceres::AutoDiffCostFunction<sCostFocalSameDeltaP2, 2, 3, 4, 3, 3>(new sCostFocalSameDeltaP2(_point, _observation)));
    }

    cv::Point3d point;
    cv::Point2d observation;
};

struct sCostFocalSameDeltaFull {
    sCostFocalSameDeltaFull(const cv::Point3f _point,
                            const cv::Point2f _observation);

    template <typename T>
    bool operator()(const T* const param_K,
                    const T* const delta,
                    const T* const rvecs,
                    const T* const tvecs,
                    T* residuals) const ;

    static ceres::CostFunction* Create(const cv::Point3f _point,
                                       const cv::Point2f _observation) {
        return (new ceres::AutoDiffCostFunction<sCostFocalSameDeltaFull, 2, 3, 8, 3, 3>(new sCostFocalSameDeltaFull(_point, _observation)));
    }

    cv::Point3d point;
    cv::Point2d observation;
};

struct sCostFocalDiffDeltaP2 {
    sCostFocalDiffDeltaP2(const cv::Point3f _point,
                          const cv::Point2f _observation);

    template <typename T>
    bool operator()(const T* const param_K,
                    const T* const delta,
                    const T* const rvecs,
                    const T* const tvecs,
                    T* residuals) const ;

    static ceres::CostFunction* Create(const cv::Point3f _point,
                                       const cv::Point2f _observation) {
        return (new ceres::AutoDiffCostFunction<sCostFocalDiffDeltaP2, 2, 4, 4, 3, 3>(new sCostFocalDiffDeltaP2(_point, _observation)));
    }

    cv::Point3d point;
    cv::Point2d observation;
};

struct sCostFocalDiffDeltaFull {
    sCostFocalDiffDeltaFull(const cv::Point3f _point,
                            const cv::Point2f _observation);

    template <typename T>
    bool operator()(const T* const param_K,
                    const T* const delta,
                    const T* const rvecs,
                    const T* const tvecs,
                    T* residuals) const ;

    static ceres::CostFunction* Create(const cv::Point3f _point,
                                       const cv::Point2f _observation) {
        return (new ceres::AutoDiffCostFunction<sCostFocalDiffDeltaFull, 2, 4, 8, 3, 3>(new sCostFocalDiffDeltaFull(_point, _observation)));
    }

    cv::Point3d point;
    cv::Point2d observation;
};

class cCalibration{
public:
    cCalibration();
    ~cCalibration();

    bool setData(const cv::Size _size_image,
                 const std::vector<points3d>& _objects,
                 const std::vector<points2d>& _images);

    bool initializeParameters(const double aspectratio = 1.0);
    bool optimizeParameters(const bool sameFocal, const bool deltaFull);

    void printK() const {param.printK();}
    void printDelta() const {param.printDelta();}
    void printIntrinsics() const {param.printIntrinsics();}
    void printExtrinsics() const {param.printExtrinsics();}
    void printRMSE() const {std::cout << "RMSE: " << rmse << std::endl;}
    void print() const {param.print(); printRMSE();}

    bool saveParam(const std::string filename) const ;
    bool loadParam(const std::string filename);
private:
    void setCostFunctionIndex(const bool sameFocal, const bool deltaFull);
    std::vector<ReturnCostFunction> ptr_CostFunctions;
    int index_CostFunction;

public:
    sData data;
    sParameter param;
    double rmse;
};
} // end of namespace calibration

