/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   AffinityMatrix.h
@brief  to compute affinity matrix for TRDM.
@author Yuji Oyamada and Hideaki Uchiyama
*/

#pragma once

#include "opencv2/opencv.hpp"
#include <Eigen/Core>
#include <Eigen/Sparse>

//! computes the distance between two points \c p0 and \c p1.
template <typename T>
T distance(
    const cv::Point_<T>& p0,
    const cv::Point_<T>& p1
);

//! computes the ratio between two distances \c dist0 and \c dist1.
template <typename T>
T computeDistanceRatio(
    const T dist0,
    const T dist1
);

class Affinity
{
public:
    //! Default constructor.
    Affinity();
    //! Destructor.
    ~Affinity();


    ///
    /// \brief computeAffinity computes pairwise affinity given reference pointsets \c pointsReference and query pointset \c pointsQuery and stores into \c matAffinity.
    /// \param pointsReference  reference pointsets.
    /// \param pointsQuery      query pointset.
    /// \param matAffinity      the result affinity matrix.
    ///
    void computeAffinity(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        Eigen::SparseMatrix<float>& matAffinity
    );

    ///
    /// \brief computeAffinity computes pairwise affinity given reference pointset \c pointsReference and query pointset \c pointsQuery and stores into \c matAffinity.
    /// \param pointsReference  reference pointset.
    /// \param pointsQuery      query pointset.
    /// \param matAffinity      the result affinity matrix.
    ///
    void computeAffinity(
        const std::vector<cv::Point2f>& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        Eigen::SparseMatrix<float>& matAffinity
    );

    ///
    /// computes unary affinity given reference feature vectorsets \c featureReference and query vectorset \c featureQuery and pairwise affinity given reference pointsets \c pointsReference and query pointset \c pointsQuery and stores into \c matAffinity.
    /// \param pointsReference  reference pointsets.
    /// \param pointsQuery      query pointset.
    /// \param featreReference  reference feature vectorsets.
    /// \param featreQuery      query feature vectorset.
    /// \param matAffinity      the result affinity matrix.
    ///
    void computeAffinity(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        const std::vector< std::vector<cv::Mat> >& featuresReference,
        const std::vector<cv::Mat>& featuresQuery,
        Eigen::SparseMatrix<float>& matAffinity
    );

    ///
    /// \brief init initialize all member variables.
    /// \param pointsReference  reference pointsets.
    /// \param pointsQuery      query pointset.
    /// \param matAffinity      the result affinity matrix.
    ///
    void init(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        Eigen::SparseMatrix<float>& matAffinity
    );

    ///
    /// \brief init initialize all member variables.
    /// \param pointsReference  reference pointset.
    /// \param pointsQuery      query pointset.
    /// \param matAffinity      the result affinity matrix.
    ///
    void init(
        const std::vector<cv::Point2f>& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        Eigen::SparseMatrix<float>& matAffinity
    );

    ///
    /// \brief set sets the current affinity information into affinity matrix \c matAffinity.
    /// \param matAffinity      the result affinity matrix.
    ///
    void set(
        Eigen::SparseMatrix<float>& matAffinity
    ) const ;

    ///
    /// \brief addUnaryTerm computes unary affinity given reference feature vectorsets \c featureReference and query vectorset \c featureQuery.
    /// \param featreReference  reference feature vectorsets.
    /// \param featreQuery      query feature vectorset.
    ///
    void addUnaryTerm(
        const std::vector< std::vector<cv::Mat> >& featuresReference,
        const std::vector<cv::Mat>& featuresQuery
    );

    ///
    /// \brief addPairwiseTerm computes pairwise affinity given reference pointsets \c pointsReference and query pointset \c pointsQuery.
    /// \param pointsReference  reference pointsets.
    /// \param pointsQuery      query pointset.
    ///
    void addPairwiseTerm(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery
    );

    ///
    /// \brief addPairwiseTerm computes pairwise affinity given reference pointset \c pointsReference and query pointset \c pointsQuery.
    /// \param pointsReference  reference pointsets.
    /// \param pointsQuery      query pointset.
    ///
    void addPairwiseTerm(
        const std::vector<cv::Point2f>& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery
    );

    //! sets the flag indicating how to compute affinity \c methodAffinity.
    void MethodAffinity(const int _methodAffinity){methodAffinity = _methodAffinity;}
    //! sets the threshold value to acclerate the sparseness of the affinity matrix \c distThreshold.
    void DistThreshold(const float _distThreshold){distThreshold = _distThreshold;}

private:
    int numPointsetsReference;              //!< The number of reference point sets.
    std::vector<int> numPointsReference;    //!< The number of points in the reference point sets.
    int numPointsQuery;                     //!< The number of query points.
    std::vector<int> numCorrespondences;    //!< The number of all potential correspondences.
    std::vector<int> startIndex;            //!< The first indices of rows/cols corresponding to markers such that the indices of rows and columns corresponding to i-th marker are startIndex[i].
    float distThreshold;                    //!< The threshold value to acclerate the sparseness of the affinity matrix. If an element M(i,j) is less than this threshold \c distThreshold, the element is not stored in the affinity matrix.
    int methodAffinity;                     //!< The flag indicating how to compute affinity.
    std::vector< Eigen::Triplet<float> > tripletList;   //!< The triplet information for fast Eigen::SparseMatrix construction.
    long countNonZero;                      //!< The number of non-zero elements.
};

