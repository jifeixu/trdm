/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Combination.cpp
@brief  to compute combinations.
@author Hideaki Uchiyama
*/

#include "Combination.h"

/*!
@brief		get n C m combinations
@param[in]	n		n of n C m
@param[in]	m		m of n C m
@retval		list of combinations
*/
std::vector< std::vector<int> > CCombination::GetCombination(const int n, const int m)
{
	std::vector< std::vector<int> > data;

	int k[200];
	int nest = 0;
	int column = 1;

	compute(nest, column, m, n - m, k, data);

	return data;
}

/*!
@brief		get n C m combinations with all rotations
@param[in]	n		n of n C m
@param[in]	m		m of n C m
@retval		list of combinations
*/
std::vector< std::vector<int> > CCombination::GetAllRotatedCombination(const int n, const int m)
{
	std::vector< std::vector<int> > data;

	int k[256];			// 200 is meaningless
	int nest = 0;
	int column = 1;

	std::vector< std::vector<int> > nCm;

	compute(nest, column, m, n - m, k, nCm);

	for (int i = 0; i < static_cast<int>(nCm.size()); i++){
		for (int j = 0; j < m; j++){
			std::vector<int> tmp(m);
			for (int k = 0; k < m; k++){
				if (j + k < m){
					tmp[k] = nCm[i][j + k];
				}
				else{
					tmp[k] = nCm[i][j + k - m];
				}
			}
			data.push_back(tmp);
		}
	}

	return data;
}

/*!
@brief		compute combinations
*/
void CCombination::compute(int nest, int column, int n1, int n2, int k[], std::vector< std::vector<int> > &result)
{
	for (int i = nest + 1; i <= n2 + column; i++){
		k[column] = i;

		if (n1 != column){
			compute(i, column + 1, n1, n2, k, result);
		}
		else{
			std::vector<int> tmp;
			for (int j = 1; j <= n1; j++){
				tmp.push_back(k[j] - 1);
			}
			result.push_back(tmp);
		}
	}
}
