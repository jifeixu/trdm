/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   MarkerGeneration.cpp
@brief  core to generate, save, and load random dot markers.
@author Yuji Oyamada
*/
#include <cassert>
#include <opencv2/opencv.hpp>

#include "MarkerGenerator.h"

double a4_width = 8.3;      // 8.3inch = 210mm
double a4_height = 11.7;    // 11.7inch = 297mm

namespace Scale{
double mm2inch(const double mm){
    return mm/25.4;
}
double inch2mm(const double inch){
    return 25.4*inch;
}
double inch2pixel(const double inch, const double dpi){
    return dpi*inch;
}
double mm2pixel(const double mm, const double dpi){
    return inch2pixel(dpi, mm2inch(mm));
}
double pixel2inch(const double pixel, const double dpi){
    return pixel/dpi;
}
double pixel2mm(const double pixel, const double dpi){
    return inch2mm(pixel2inch(pixel, dpi));
}
} // end of namespace Scale

RandomDotsMarkerGenerator::RandomDotsMarkerGenerator(void){
	Init();
}

RandomDotsMarkerGenerator::~RandomDotsMarkerGenerator(void){
}

void RandomDotsMarkerGenerator::Init(){
    m_dpi = 300;
    SetWidth(Scale::inch2mm(a4_width));
    SetHeight(Scale::inch2mm(a4_height));
    SetNumOfDots(50);
    int radius = 5;
    SetDotRadius(radius);
    SetDotsMargin(3*radius);
    SetDotsColor("black");
    SetBgColor("white");
    engine = std::default_random_engine(rd());
}

void RandomDotsMarkerGenerator::ShowCurrentValue(
    const int caseVariable
)
{
    switch(caseVariable)
    {
        case MARKER_WIDTH:
            std::cout << "input image width (default = " << GetWidth() << " mm): ";
            break;
        case MARKER_HEIGHT:
            std::cout << "input image height (default = " << GetHeight() << " mm): ";
            break;
        case MARKER_NUMOFDOTS:
            std::cout << "input number of dots (default = " << GetNumOfDots() << "): ";
            break;
        case MARKER_DOTSMARGIN:
            std::cout << "input dots margin (default = " << GetDotMargin() << " mm): ";
            break;
        case MARKER_DOTRADIUS:
            std::cout << "input dot's radius (default = " << GetDotRadius() << " mm): ";
            break;
        case MARKER_DOTSCOLOR:
            std::cout << "input dot's color (default = " << GetDotColor() << "): ";
            break;
        case MARKER_BGCOLOR:
            std::cout << "input background color (default = " << GetBgColor() << "): ";
            break;
    }
}

void RandomDotsMarkerGenerator::SetCurrentValue(
    const int caseVariable,
    const std::string varStr
)
{
    int varInt = 0;
    if(caseVariable < MARKER_DOTSCOLOR)
    {
        varInt = atoi(varStr.c_str());
    }

    switch(caseVariable)
    {
        case MARKER_WIDTH:
            SetWidth(varInt);
            break;
        case MARKER_HEIGHT:
            SetHeight(varInt);
            break;
        case MARKER_NUMOFDOTS:
            SetNumOfDots(varInt);
            break;
        case MARKER_DOTSMARGIN:
            SetDotsMargin(varInt);
            break;
        case MARKER_DOTRADIUS:
            SetDotRadius(varInt);
            break;
        case MARKER_DOTSCOLOR:
            SetDotsColor(varStr);
            break;
        case MARKER_BGCOLOR:
            SetBgColor(varStr);
            break;
    }
}

void RandomDotsMarkerGenerator::SetAllVariables(void)
{
    std::string varStr;
    for(int var = 0; var < MARKER_NUMPROPERTY; ++var)
    {
        ShowCurrentValue(var);
        std::getline(std::cin, varStr);
        if(varStr != "")
        {
            SetCurrentValue(var, varStr);
        }
    }
}

void RandomDotsMarkerGenerator::ShowAllVariables(void)
{
    std::cout << "Current settings:" << std::endl;
    std::cout << "Marker: " << std::endl;
    std::cout << "	DPI: " << GetDpi() << std::endl;
    std::cout << "	Size [mm]: " << GetWidth() << "x" << GetHeight() << std::endl;
    std::cout << "	Size [pixel]: " << Scale::mm2pixel(GetWidth(), GetDpi()) << "x" << Scale::mm2pixel(GetHeight(), GetDpi()) << std::endl;
    std::cout << "	Number of dots: " << GetNumOfDots() << std::endl;
    std::cout << "	Background color: " << GetBgColor() << std::endl;
    std::cout << "Dot:" << std::endl;
    std::cout << "	Radius: " << GetDotRadius() << std::endl;
    std::cout << "	Dot color: " << GetDotColor() << std::endl;
    std::cout << "	Dot margin: " << GetDotMargin() << std::endl;
}

void RandomDotsMarkerGenerator::MakeMarkerSets(
    const std::string basename,
    const int numMarker
)
{
    ShowAllVariables();
    std::vector<Eigen::MatrixXd> markers;
    for(size_t i = 0; i < numMarker; ++i)
    {
        std::cout << "generate " << i << "-th marker..." << std::endl;
        MakeMarker(basename + std::to_string(i));
        markers.push_back(m_points);
    }
    std::cout << "save the generated markers as xml" << std::endl;
    SaveMarkersAsXml(basename+".xml", markers);
}

void RandomDotsMarkerGenerator::MakeMarker(const std::string basename)
{
    // init
    CheckImageSize();
    CheckNumOfDots();

    // generate a set of dots
    GenerateDots(m_points);

    // save as an image
    SaveMarkerAsImage(basename+".pdf");

    // save as an xml
    SaveMarkerAsXml(basename+".xml");
}

void RandomDotsMarkerGenerator::SaveMarkerAsImage(const std::string name)
{
    // draw image boundary
    int w = Scale::mm2pixel(m_imageWidth, m_dpi);
    int h = Scale::mm2pixel(m_imageHeight, m_dpi);
    int size_line = 5;

    m_img.fillColor("black");
    m_img.draw(Magick::DrawableRectangle(0, 0, w, size_line));
    m_img.draw(Magick::DrawableRectangle(0, h-size_line, w, h));
    m_img.draw(Magick::DrawableRectangle(0, 0, size_line, h));
    m_img.draw(Magick::DrawableRectangle(w-size_line, 0, w, h));

    // draw all dots
    m_img.backgroundColor(m_bgColor);
    MagickCore::SetImageBackgroundColor(m_img.image());

    m_img.fillColor(m_dotColor);
    double x, y, r = Scale::mm2pixel(m_dotRadius, m_dpi);

    for(size_t i = 0; i < m_points.cols(); ++i){
        x = Scale::mm2pixel(m_points(0, i), m_dpi);
        y = Scale::mm2pixel(m_points(1, i), m_dpi);
        m_img.draw(Magick::DrawableCircle(x, y, x+r, y));
    }

    DrawScale();

    // draw settings
    std::stringstream strm;
    strm << m_points.cols() << "dots ";
    strm << "on " << m_imageWidth << "mm ";
    strm << "x " << m_imageHeight << "mm marker. ";
    strm << "w/ radius=" << m_dotRadius << "mm";
    m_img.fontPointsize(32);
    m_img.strokeColor(m_dotColor);
    m_img.fillColor(m_dotColor);
    m_img.draw(Magick::DrawableText(10, Scale::mm2pixel(m_imageHeight, m_dpi)-10,
                                    strm.str()));

    // save
    m_img.write(name.c_str());
}

void RandomDotsMarkerGenerator::SaveText(const std::string name)
{
	std::ofstream ofs(name.c_str());
	ofs << m_imageWidth << " " << m_imageHeight << std::endl;
    ofs << m_points.cols() << std::endl;
    for(size_t i = 0; i < m_points.cols(); ++i)
	{
        ofs << m_points.col(i).transpose() << ";" << std::endl;
	}
	ofs.close();
}

void RandomDotsMarkerGenerator::SaveMarkerAsXml(
    const std::string fileName
)
{
    cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

    fs << "image_width" << int(m_imageWidth);
    fs << "image_height" << int(m_imageHeight);
    fs << "num_points" << int(m_numOfDots);
    fs << "points" << "[";
    for(size_t i = 0; i < m_numOfDots; ++i){
        fs << "[:";
        for(size_t j = 0; j < 2; ++j){
            fs << m_points(j, i);
        }
        fs << "]";
    }
    fs << "]";
    fs.release();
}

void RandomDotsMarkerGenerator::SaveMarkersAsXml(
    const std::string fileName,
    const std::vector<Eigen::MatrixXd>& markers
)
{
    std::cout << "save as " << fileName << std::endl;
    cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

    fs << "num_markers" << int(markers.size());
    fs << "image_width" << int(m_imageWidth);
    fs << "image_height" << int(m_imageHeight);
    fs << "num_points" << int(m_numOfDots);
    fs << "markers" << "[";
    for(auto marker = markers.cbegin(); marker != markers.cend(); ++marker){
        fs << "[:";
        for(size_t i = 0; i < marker->cols(); ++i){
            fs << "[:";
            for(size_t j = 0; j < 2; ++j){
                fs << (*marker)(j, i);
            }
            fs << "]";
        }
        fs << "]";
    }
    fs << "]";
    fs.release();
}


bool RandomDotsMarkerGenerator::LoadMarkerFromXml(
    const std::string fileName,
    int& width,
    int& height,
    Eigen::MatrixXd& points
)
{
    cv::FileStorage fs(fileName, cv::FileStorage::READ);
    if(!fs.isOpened()){
        std::cerr << "Load error of " << fileName << std::endl;
        return false;
    }

    width = int(fs["image_width"]);
    height = int(fs["image_height"]);
    int num_points = int(fs["num_points"]);
    std::cout << "image size: (" << width << ", " << height << ")" << std::endl;
    std::cout << "#points: " << num_points << std::endl;

    points = Eigen::MatrixXd(2, num_points);
    auto fn = fs["points"];
    int i = 0, j = 0;
    double tmp;
    for(auto it_m = fn.begin(); it_m != fn.end(); ++it_m, ++i)
    {
        j = 0;
        for(auto it_p = (*it_m).begin(); it_p != (*it_m).end(); ++it_p, ++j)
        {
            *it_p >> tmp;
            points(j, i) = tmp;
        }
    }

}


void RandomDotsMarkerGenerator::CheckImageSize()
{
    assert(m_imageWidth > 2*m_dotMargin);
    assert(m_imageHeight > 2*m_dotMargin);

    // set image canvas
    if(m_img.size() == Magick::Geometry(0,0)){
        m_img.size(Magick::Geometry(Scale::mm2pixel(m_imageWidth, m_dpi),
                                    Scale::mm2pixel(m_imageHeight, m_dpi)));
        m_img.resolutionUnits(Magick::PixelsPerInchResolution);
        m_img.density(Magick::Geometry(m_dpi, m_dpi));
	}
    else if((m_img.size().width() != m_imageWidth) || (m_img.size().height() != m_imageHeight)){
        m_img.resize(Magick::Geometry(Scale::mm2pixel(m_imageWidth, m_dpi),
                                      Scale::mm2pixel(m_imageHeight, m_dpi)));
        m_img.backgroundColor(m_bgColor);
        MagickCore::SetImageBackgroundColor(m_img.image());
	}

}

void RandomDotsMarkerGenerator::CheckNumOfDots()
{
    if(m_indices.size() != 0){
        m_indices.clear();
	}
}

void RandomDotsMarkerGenerator::GenerateDots(Eigen::MatrixXd& points)
{
    assert(m_points.cols() == m_numOfDots);
    // set random value generator
    distX = std::uniform_real_distribution<>(m_dotMargin, m_imageWidth-m_dotMargin);
    distY = std::uniform_real_distribution<>(m_dotMargin, m_imageHeight-m_dotMargin);

	size_t i, j;
	int collision = 0;
    double diff_th = static_cast<double>(m_dotMargin);

    m_indices.clear();
    for(i = 0; i < m_numOfDots || collision > 20; ++i)	// generate dot
	{
        m_points(0, i) = distX(engine);
        m_points(1, i) = distY(engine);

        // avoid overlap of dots
        for(j = 0; j < i; ++j){
            if((m_points.col(i)-m_points.col(j)).norm() < diff_th){
                break;
			}
		}

        if(j < i){
			++collision;
			--i;
		}
        else{
			collision = 0;
            m_indices.push_back(i);
		}
	}
}

void RandomDotsMarkerGenerator::DrawScale()
{
    m_img.strokeWidth(1);

    int interval = 50.0; // mm scale
    double length_bar = 0.2*m_dpi;

    // add scale on x axis
    m_img.strokeColor("red");
    for(int x = interval; x < m_imageWidth; x+=interval){
        std::string x_str = std::to_string(x)+"mm";
        double x_pixel = Scale::mm2pixel(x, m_dpi);
        m_img.draw(Magick::DrawableLine(x_pixel, 0, x_pixel, length_bar));
        m_img.draw(Magick::DrawableText(x_pixel, length_bar, x_str));
    }
    // add scale on y axis
    m_img.strokeColor("blue");
    for(int y = interval; y < m_imageHeight; y+=interval){
        std::string y_str = std::to_string(y)+"mm";
        double y_pixel = Scale::mm2pixel(y, m_dpi);
        m_img.draw(Magick::DrawableLine(0, y_pixel, length_bar, y_pixel));
        m_img.draw(Magick::DrawableText(0, y_pixel, y_str));
    }
}

void RandomDotsMarkerGenerator::DrawOrigin()
{
    std::string color_o = "black";
    std::string color_x = "red";
    std::string color_y = "green";
    std::string color_z = "blue";
	int x2 = m_imageWidth/2, y2 = m_imageHeight/2;
	int size_tri = std::min(x2,y2)/20, size_line = 5;
	std::list<Magick::Coordinate> pts_triangle;
// x axis
    m_img.fillColor(color_x);
    m_img.draw(Magick::DrawableRectangle(0, 0, x2-size_tri, size_line));
    pts_triangle.push_back(Magick::Coordinate(x2,0));
    pts_triangle.push_back(Magick::Coordinate(x2-size_tri, size_tri));
    pts_triangle.push_back(Magick::Coordinate(x2-size_tri,-size_tri));
    m_img.draw(Magick::DrawablePolygon(pts_triangle));
    pts_triangle.clear();
// y axis
    m_img.fillColor(color_y);
    m_img.draw(Magick::DrawableRectangle(0, 0, size_line, y2-size_tri));
    pts_triangle.push_back(Magick::Coordinate(0,y2));
    pts_triangle.push_back(Magick::Coordinate(size_tri, y2-size_tri));
    pts_triangle.push_back(Magick::Coordinate(-size_tri, y2-size_tri));
    m_img.draw(Magick::DrawablePolygon(pts_triangle));
// image origin
    m_img.fillColor(color_o);
    m_img.draw(Magick::DrawableCircle(0, 0, 2*m_dotRadius, 2*m_dotRadius));
}

