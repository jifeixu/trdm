/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   LLAH.h
@brief  the core implementation of TRDM.
@author Yuji Oyamada and Hideaki Uchiyama
*/


#pragma once

#include "opencv2/opencv.hpp"
#include "Combination.h"

#include <vector>
#include <map>
#include <string>
#include <fstream>

//#define VTABLE		//!< use vector for hash table

struct sRetrieval
{
    sRetrieval(int _id_query = -1,
               int _id_marker = -1,
               int _id_point = -1):
        id_query(_id_query),
        id_marker(_id_marker),
        id_point(_id_point)
    {}
    ~sRetrieval(){}
    int id_query;
    int id_marker;
    int id_point;
};

struct sMatching {
    sMatching(
        const cv::Point2f& _query = cv::Point2f(),
        const cv::Point2f& _reference = cv::Point2f(),
        const int _id_marker = -1,
        const int _id_point = -1
    ): query(_query),
       reference(_reference),
       id_marker(_id_marker),
       id_point(_id_point)
    {}
    ~sMatching(){}

    bool operator<(const sMatching& rhs)const{
        if(id_marker < rhs.id_marker)
        {
            return true;
        }
        else
        {
            if(id_point < rhs.id_point)
            {
                return true;
            }
        }
        return false;
    }
    cv::Point2f query;
    cv::Point2f reference;
    int id_marker;
    int id_point;
};

/*!
@struct		LLAHParam
@brief		Parameters in LLAH
*/
struct sLLAHParam
{
	int GetQLevel(const float val) const;
	void ComputeQuantization(const std::vector<float> &data);
	bool Init(const int _n, const int _m, const int _k, const int _qbit);

	std::vector< std::vector<int> > wnCmall;	//!< nCm with all rotations
	std::vector< std::vector<int> > wnCm;		//!< nCm
	std::vector< std::vector<int> > wmCk;		//!< mCk
	int n;		//!< n of nCm
	int m;		//!< m of nCm
	int k;		//!< k is 4 (for ratio of two triangles)
	int qbit;	//!< quantization level ( represented by number of bit)
	unsigned hashsize;		//!< size of hash table (computed from qbit)
	std::vector<float> qlevel;	//!< quantization levels
};

// structure of hash table
#ifdef VTABLE
typedef std::vector<std::vector<unsigned>> htable;
#else
typedef std::map<unsigned, std::vector<unsigned> > htable;
#endif
const unsigned BITWIDTH = 16;	// first 16 bit for marker ID and second 16 bit for point ID

/*!
@struct		sortelem
@brief		use this for sorting ID by some value
*/
template <typename T>
struct sortelem
{
	sortelem(){ val = T(0); }
	bool operator<(const sortelem& r) const { return val < r.val; }
	bool operator>(const sortelem& r) const { return val > r.val; }

	unsigned id;	//!< id for value
	unsigned id2;	//!< second id for value
	T val;			//!< value for sorting
};

/*!
@struct		sMarker
@brief		marker ifnormation
*/
struct sMarker
{
	unsigned id;		//!< marker size
	cv::Mat H;			//!< homography
	int size;			//!< marker size
	int numinliers;		//!< num of inliers
	bool surface;		//!< front(true) or back(false)

	cv::Vec3d r;		//!< rotation vector
	cv::Vec3d t;		//!< translation vector
};


/*
@struct		pairelem
@brief		for CPairwiseMat
*/
template <typename T>
struct pairelem{
    bool operator==(const pairelem &data) const { return data.c == c; }
	int c;	//!< column position
	std::vector<T> vdata;	//!< elements for each column position
};

/*!
@struct		sPairwiseMat
*/
template <typename T>
class CPairwiseMat
{
public:
	void resize(const int _row, const int _col);
	void set(const int _row, const int _col, const std::vector<T> &vdata);
	const std::vector< pairelem<T> >& get(const int _row) const;
	bool load(std::ifstream &in);
	void save(std::ofstream &out) const;

private:
	std::vector< std::vector< pairelem<T> > > m_welem;	//!< list of rows (each row is std::vector< pairelem<T> >
	int m_row;		//!< num of rows
	int m_col;		//!< num of columns
};

/*!
@struct		sMatch
@brief		matching information
*/
struct sMatch{

	/*!
	@struct		sID
	@brief		ID information
	*/
	struct sID{
		sID() { score = -1.0f; }
        bool operator==(const sID &data) const { return data.ptID == ptID && data.markerID == markerID; }
		int ptID;
		int markerID;
		float score;
	};

	sID gt;		//!< ground truth
	std::vector<sID> vdst;	//!< result
};

/*!
@struct		sEval
@brief		evaluation data
*/
struct sEval
{
	sEval() { noise = match = correct = reference = time = 0; precision = recall = 0.f; }
    bool operator<(const sEval &r) const { return noise < r.noise;  }
	int noise;
	int match;
	int correct;
	int reference;
	int time;
	float precision;
	float recall;
};

/*!
@class		CLLAH
@brief		class for LLAH
*/
class CLLAH
{
public:
    CLLAH();
	~CLLAH();

    bool load_config(const std::string filename);
    bool init(const std::string filename);
    bool init(const sLLAHParam param);
    bool build(const std::vector< std::vector< cv::Point2f > >& markers);
    void find(const std::vector< cv::Point2f >& vpt);
    void find(const std::vector< cv::Point2f >& vpt,
              std::vector<sRetrieval>& matches);
    bool save_matches(const std::string filename);

    void draw_points(cv::Mat& img);
    void draw_markers(cv::Mat& img,
                      const std::vector<cv::Point2f>& pts);

private:

    bool loadMarkers(const std::string &name);
	void saveMarkers(const std::string &name) const;
	bool loadHashTable(const std::string &name);
	void saveHashTable(const std::string &name) const;
	bool loadLLAHParam(const std::string &name);
	void saveLLAHParam(const std::string &name) const;
	bool loadPairwise(const std::string &name);
    void savePairwise(const std::string &name) const;
	bool searchID(const std::vector<int> &vdesc, unsigned &id) const;
	void searchID(const std::vector<int> &vdesc, std::vector<sortelem<int>> &vid) const;
	void findID(const int index, std::vector<unsigned> &vid) const;
	unsigned computeID(const unsigned short markerID, const unsigned short ptID) const;
	unsigned getMarkerID(const unsigned id) const;
	unsigned getPtID(const unsigned id) const;
	void addHashtable(const int index, const unsigned &id);
	void computePairwise(const std::vector< std::vector<int> > &wdesc, const std::vector< std::vector<int> > &wneighbor, CPairwiseMat<int> &pairwise) const;
	void computeHashtable(void);
	void computeDescriptors(const std::vector< std::vector<int> > &wnCm, const std::vector< std::vector<int> > &wmCk, const std::vector< cv::Point2f > &vpt, std::vector< std::vector< int > > &wdesc, std::vector< std::vector<int> > &wneighbor) const;
	void computeRawDescriptors(const std::vector< std::vector<int> > &wnCm, const std::vector< std::vector<int> > &wmCk, const std::vector< cv::Point2f > &vpt, std::vector< float > &vraw, std::vector< std::vector<int> > &wneighbor) const;
	float computeAffineInvariant(const cv::Point2f &p1, const cv::Point2f &p2, const cv::Point2f &p3, const cv::Point2f &p4) const;
	float computeTriangle(const cv::Point2f &p1, const cv::Point2f &p2, const cv::Point2f &p3) const;
	void sortbyAngle(const cv::Point2f &pt, const std::vector< cv::Point2f > &vpt, std::vector<int> &windex) const;
	void sortbyAngle2(const cv::Point2f &pt, const std::vector< cv::Point2f > &vpt, std::vector<int> &windex) const;
	void getNeighbors(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const;
	void getNeighbors3(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const;
	void getNeighbors2(const int num, const std::vector< cv::Point2f > &vpt, std::vector< std::vector<int> > &windex) const;
	
private:
	std::vector< std::vector< cv::Point2f > > m_wrefpt;		//!< reference points
	htable m_table;				//!< hash table for searching
	sLLAHParam m_param;			//!< parameters for LLAH
	int m_markersize;			//!< size of square marker

	cv::Mat m_A;	//!< internal paramters
	cv::Mat m_D;	//!< distortion parameters

	std::vector< CPairwiseMat<int> > m_vpairwise;	//!< pairwise data
	bool m_bmymethod;	//!< use my method
	bool m_bhungarian;	//!< use hungarian
	bool m_bpairwise;	//!< use pairwise
	bool m_bbackside;	//!< save backside
	bool m_bpnp;			//!< use pnp

	std::ofstream m_result;		//!< just for saving result

    int m_num_queries;
    std::vector< sMatching > m_matchings;

public:
    std::vector<cv::Matx33d> m_H;
    std::vector<int> m_found;
};

