/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Calibration.cpp
@brief  the core implementation for camera calibration.
@author Yuji Oyamada
*/

#include <cassert>

#include "Calibration.h"


namespace calibration{
///////////////////////////////////////
/// struct sData
///
sData::sData(){
}

sData::~sData(){
}

bool sData::set(
        const cv::Size _size_image,
        const std::vector<points3d>& _objects,
        const std::vector<points2d>& _images
        ){
    std::cout << "Set new data..." << std::endl;

    objects.clear();
    images.clear();
    num_points.clear();

    // set size
    size_image = _size_image;
    std::cout << "  image resolution: (" << size_image.width << "x" << size_image.height << ")" << std::endl;

    // set points
    assert(_objects.size() == _images.size());
    num_images = _objects.size();
    objects = std::vector<points3d>(num_images);
    images = std::vector<points2d>(num_images);
    std::cout << "  #images: " << num_images << std::endl;
    for(size_t i = 0; i < num_images; ++i){
        assert(_objects[i].size() == _images[i].size());
        num_points.push_back(_objects[i].size());

        objects[i] = points3d(num_points[i]);
        images[i] = points2d(num_points[i]);
        for(size_t j = 0; j < num_points[i]; ++j){
            objects[i][j] = _objects[i][j];
            images[i][j] = _images[i][j];
        }
//        std::cout << "    #points in image[" << i << "]: " << num_points[i] << std::endl;
    }

    return true;
}

///////////////////////////////////////
/// struct sParameter
///
sParameter::sParameter():
    K(cv::Mat::eye(3, 3, CV_64F)),
    delta(cv::Mat::zeros(8, 1, CV_64F)){
}

sParameter::~sParameter(){
}

void sParameter::printK() const {
    std::cout << "K" << K << std::endl;
}

void sParameter::printDelta() const {
    std::cout << "delta" << delta.t() << std::endl;
}

void sParameter::printExtrinsics() const {
    for(size_t i = 0; i < rvecs.size(); ++i)
    {
        std::cout << "  r[" << i << "] " << rvecs[i].t() << std::endl;
        std::cout << "  t[" << i << "] " << tvecs[i].t() << std::endl;
    }
}

void sParameter::print() const {
    printIntrinsics();
    printExtrinsics();
}

bool sParameter::save(const std::string filename) const {
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);
    if (!fs.isOpened()){
        std::cerr << "open " + filename + " failed..." << std::endl;
        return false;
    }

    fs << "Intrinsic" << K;
    fs << "Distortion" << delta;

    size_t numImages = rvecs.size();
    fs << "NumOfExtrinsics" << int(numImages);
    fs << "Extrinsics" << "[";
    for(size_t i = 0; i < numImages; ++i){
        fs << "{";
        fs << "R" << rvecs[i];
        fs << "t" << tvecs[i];
        fs << "}";
    }
    fs << "]";
    fs.release();

    return true;
}

bool sParameter::load(const std::string filename){
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        std::cerr << "open " + filename + " failed..." << std::endl;
        return false;
    }

    fs["Intrinsic"] >> K;
    fs["Distortion"] >> delta;

    int numImages;
    fs["NumOfExtrinsics"] >> numImages;

    cv::FileNode nodes = fs["Extrinsics"];
    rvecs = std::vector<cv::Mat>(numImages);
    tvecs = std::vector<cv::Mat>(numImages);

    size_t i = 0;
    for(auto it = nodes.begin(); it != nodes.end(); ++it, ++i)
    {
        (*it)["R"] >> rvecs[i];
        (*it)["t"] >> tvecs[i];
    }
    fs.release();

    return true;
}
///////////////////////////////////////
/// struct sCostFocalSameDeltaP2
///
sCostFocalSameDeltaP2::sCostFocalSameDeltaP2(const cv::Point3f _point,
                                             const cv::Point2f _observation):
    point(_point), observation(_observation){}

template <typename T>
bool sCostFocalSameDeltaP2::operator()(const T* const param_K,
                                       const T* const delta,
                                       const T* const rvecs,
                                       const T* const tvecs,
                                       T* residuals) const {
    T _point[3] = {T(point.x), T(point.y), T(point.z)};
    T p[3];

    // rotate the point
    ceres::AngleAxisRotatePoint(rvecs, _point, p);

    // translate the point
    p[0] += tvecs[0];
    p[1] += tvecs[1];
    p[2] += tvecs[2];

    // project the point
    T xp = p[0] / p[2];
    T yp = p[1] / p[2];

    // distort the point
    const T& k1 = delta[0];
    const T& k2 = delta[1];
    const T& p1 = delta[2];
    const T& p2 = delta[3];
    T xpyp = xp*yp;
    T xp2= xp*xp;
    T yp2 = yp*yp;
    T r2 = xp2+ yp2;
    T r4 = r2*r2;
    T xpp = xp + k1 * r2 * xp +
                 k2 * r4 * xp +
                 p1 * T(2.0)*xpyp +
                 p2 * (r2 + T(2.0)*xp2);
    T ypp = yp + k1 * r2 * yp +
                 k2 * r4 * yp +
                 p1 * (r2 + T(2.0)*yp2) +
                 p2 * T(2.0)*xpyp;

    // compute the residual
    T u = T(param_K[0]) * xpp + T(param_K[1]);
    T v = T(param_K[0]) * ypp + T(param_K[2]);

    residuals[0] = T(observation.x) - u;
    residuals[1] = T(observation.y) - v;

    return true;
}

///////////////////////////////////////
/// struct sCostFocalSameDeltaFull
///
sCostFocalSameDeltaFull::sCostFocalSameDeltaFull(const cv::Point3f _point,
                                                 const cv::Point2f _observation):
    point(_point), observation(_observation){}

template <typename T>
bool sCostFocalSameDeltaFull::operator()(const T* const param_K,
                                         const T* const delta,
                                         const T* const rvecs,
                                         const T* const tvecs,
                                         T* residuals) const {
    T _point[3] = {T(point.x), T(point.y), T(point.z)};
    T p[3];

    // rotate the point
    ceres::AngleAxisRotatePoint(rvecs, _point, p);

    // translate the point
    p[0] += tvecs[0];
    p[1] += tvecs[1];
    p[2] += tvecs[2];

    // project the point
    T xp = p[0] / p[2];
    T yp = p[1] / p[2];

    // distort the point
    const T& k1 = delta[0];
    const T& k2 = delta[1];
    const T& p1 = delta[2];
    const T& p2 = delta[3];
    const T& k3 = delta[4];
    const T& k4 = delta[5];
    const T& k5 = delta[6];
    const T& k6 = delta[7];
    T xpyp = xp*yp;
    T xp2= xp*xp;
    T yp2 = yp*yp;
    T r2 = xp2+ yp2;
    T r4 = r2*r2;
    T r6 = r2*r4;
    T distortion = (T(1.0) + k1*r2 + k2*r4 + k3*r6) / (T(1.0) + k4*r2 + k5*r4 + k6*r6);
    T xpp = xp * distortion +
            p1 * T(2.0)*xpyp +
            p2 * (r2 + T(2.0)*xp2);
    T ypp = yp * distortion +
            p1 * (r2 + T(2.0)*yp2) +
            p2 * T(2.0)*xpyp;

    // compute the residual
    T u = T(param_K[0]) * xpp + T(param_K[1]);
    T v = T(param_K[0]) * ypp + T(param_K[2]);

    residuals[0] = T(observation.x) - u;
    residuals[1] = T(observation.y) - v;

    return true;
}

///////////////////////////////////////
/// struct sCostFocalDiffDeltaP2
///
sCostFocalDiffDeltaP2::sCostFocalDiffDeltaP2(const cv::Point3f _point,
                                             const cv::Point2f _observation):
    point(_point), observation(_observation){}

template <typename T>
bool sCostFocalDiffDeltaP2::operator()(const T* const param_K,
                                       const T* const delta,
                                       const T* const rvecs,
                                       const T* const tvecs,
                                       T* residuals) const {
    T _point[3] = {T(point.x), T(point.y), T(point.z)};
    T p[3];

    // rotate the point
    ceres::AngleAxisRotatePoint(rvecs, _point, p);

    // translate the point
    p[0] += tvecs[0];
    p[1] += tvecs[1];
    p[2] += tvecs[2];

    // project the point
    T xp = p[0] / p[2];
    T yp = p[1] / p[2];

    // distort the point
    const T& k1 = delta[0];
    const T& k2 = delta[1];
    const T& p1 = delta[2];
    const T& p2 = delta[3];
    T xpyp = xp*yp;
    T xp2= xp*xp;
    T yp2 = yp*yp;
    T r2 = xp2+ yp2;
    T r4 = r2*r2;
    T xpp = xp + k1 * r2 * xp +
                 k2 * r4 * xp +
                 p1 * T(2.0)*xpyp +
                 p2 * (r2 + T(2.0)*xp2);
    T ypp = yp + k1 * r2 * yp +
                 k2 * r4 * yp +
                 p1 * (r2 + T(2.0)*yp2) +
                 p2 * T(2.0)*xpyp;

    // compute the residual
    T u = T(param_K[0]) * xpp + T(param_K[2]);
    T v = T(param_K[1]) * ypp + T(param_K[3]);

    residuals[0] = T(observation.x) - u;
    residuals[1] = T(observation.y) - v;

    return true;
}

///////////////////////////////////////
/// struct sCostFocalDiffDeltaFull
///
sCostFocalDiffDeltaFull::sCostFocalDiffDeltaFull(const cv::Point3f _point,
                                                 const cv::Point2f _observation):
    point(_point), observation(_observation){}

template <typename T>
bool sCostFocalDiffDeltaFull::operator()(const T* const param_K,
                                         const T* const delta,
                                         const T* const rvecs,
                                         const T* const tvecs,
                                         T* residuals) const {
    T _point[3] = {T(point.x), T(point.y), T(point.z)};
    T p[3];

    // rotate the point
    ceres::AngleAxisRotatePoint(rvecs, _point, p);

    // translate the point
    p[0] += tvecs[0];
    p[1] += tvecs[1];
    p[2] += tvecs[2];

    // project the point
    T xp = p[0] / p[2];
    T yp = p[1] / p[2];

    // distort the point
    const T& k1 = delta[0];
    const T& k2 = delta[1];
    const T& p1 = delta[2];
    const T& p2 = delta[3];
    const T& k3 = delta[4];
    const T& k4 = delta[5];
    const T& k5 = delta[6];
    const T& k6 = delta[7];
    T xpyp = xp*yp;
    T xp2= xp*xp;
    T yp2 = yp*yp;
    T r2 = xp2+ yp2;
    T r4 = r2*r2;
    T r6 = r2*r4;
    T distortion = (T(1.0) + k1*r2 + k2*r4 + k3*r6) / (T(1.0) + k4*r2 + k5*r4 + k6*r6);
    T xpp = xp * distortion +
            p1 * T(2.0)*xpyp +
            p2 * (r2 + T(2.0)*xp2);
    T ypp = yp * distortion +
            p1 * (r2 + T(2.0)*yp2) +
            p2 * T(2.0)*xpyp;

    // compute the residual
    T u = T(param_K[0]) * xpp + T(param_K[2]);
    T v = T(param_K[1]) * ypp + T(param_K[3]);

    residuals[0] = T(observation.x) - u;
    residuals[1] = T(observation.y) - v;

    return true;
}

///////////////////////////////////////
/// class cCalibration
///
cCalibration::cCalibration(){
    ptr_CostFunctions.clear();
    ptr_CostFunctions = {(ReturnCostFunction)sCostFocalSameDeltaFull::Create,
                         (ReturnCostFunction)sCostFocalSameDeltaP2::Create,
                         (ReturnCostFunction)sCostFocalDiffDeltaFull::Create,
                         (ReturnCostFunction)sCostFocalDiffDeltaP2::Create};
    index_CostFunction = -1;
}

cCalibration::~cCalibration(){

}

bool cCalibration::setData(const cv::Size _size_image,
                           const std::vector<points3d>& _objects,
                           const std::vector<points2d>& _images){
    return data.set(_size_image, _objects, _images);
}

bool cCalibration::initializeParameters(const double aspectratio){
    // init intrinsic parameters
    param.K = cv::initCameraMatrix2D(data.objects, data.images, data.size_image, aspectratio);

    // init lens distortion parameters
    param.delta = cv::Mat::zeros(8, 1, CV_64F);

    // init extrinsic parameters
    param.rvecs.clear();
    param.rvecs = std::vector<cv::Mat>(data.num_images);
    param.tvecs.clear();
    param.tvecs = std::vector<cv::Mat>(data.num_images);
    for(size_t i = 0; i < data.num_images; ++i)
    {
        cv::solvePnP(data.objects[i], data.images[i],
                     param.K, param.delta, param.rvecs[i], param.tvecs[i]);
    }

    return true;
}

void cCalibration::setCostFunctionIndex(const bool sameFocal, const bool deltaFull){
    if(sameFocal){
        if(deltaFull){
            index_CostFunction = 0;
        }
        else{
            index_CostFunction = 1;
        }
    }
    else{
        if(deltaFull){
            index_CostFunction = 2;
        }
        else{
            index_CostFunction = 3;
        }
    }
}

bool cCalibration::optimizeParameters(const bool sameFocal, const bool deltaFull){

    setCostFunctionIndex(sameFocal, deltaFull);

    double param_K[4];
    param_K[0] = param.K.at<double>(0, 0);
    if(sameFocal){
        param_K[1] = param.K.at<double>(0, 2);
        param_K[2] = param.K.at<double>(1, 2);
    }
    else{
        param_K[1] = param.K.at<double>(1, 1);
        param_K[2] = param.K.at<double>(0, 2);
        param_K[3] = param.K.at<double>(1, 2);
    }

    ceres::Problem problem;
    ceres::LossFunction* loss = NULL;

    for(size_t i = 0; i < data.num_images; ++i){
        for(size_t j = 0; j < data.num_points[i]; ++j){
            ceres::CostFunction* cost_function = ((ceres::CostFunction* (*)(const cv::Point3f, const cv::Point2f)) ptr_CostFunctions[index_CostFunction])(data.objects[i][j], data.images[i][j]);
            problem.AddResidualBlock(cost_function, loss,
                                     param_K,
                                     param.delta.ptr<double>(0),
                                     param.rvecs[i].ptr<double>(0),
                                     param.tvecs[i].ptr<double>(0));
        }
    }
    ceres::Solver::Options options;
    options.max_num_iterations = 100;
    options.linear_solver_type = ceres::DENSE_SCHUR;
//    options.minimizer_progress_to_stdout = true;

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
//    std::cout << summary.BriefReport() << std::endl;

    param.K.at<double>(0, 0) = param_K[0];
    if(sameFocal){
        param.K.at<double>(1, 1) = param_K[0];
        param.K.at<double>(0, 2) = param_K[1];
        param.K.at<double>(1, 2) = param_K[2];
    }
    else{
        param.K.at<double>(1, 1) = param_K[1];
        param.K.at<double>(0, 2) = param_K[2];
        param.K.at<double>(1, 2) = param_K[3];
    }

    rmse = std::sqrt(2.0*summary.final_cost/(summary.num_residuals/2));
    return true;
}

bool cCalibration::saveParam(const std::string filename) const {
    return param.save(filename);
}

bool cCalibration::loadParam(const std::string filename){
    return param.load(filename);
}

} // end of namespace calibration
