# Transparent Random Dot Markers (TRDM)
This repository contains a C++ implementation of our work [Transparent Random Dot Markers (TRDM)](https://sites.google.com/view/dryujioyamada/research/transparent-random-dot-markers).  
From the original code for paper submission, I simplified the structure and made it as friendly as possible.

## Dependencies
The following dependencies are required to run the code.
- [OpenCV](https://opencv.org/) (for image processing and video capture)
- [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) (for spectral method)
- Optional
  - [OpenMP](https://www.openmp.org/) (for spectral method)
  - [Ceres-solver](http://ceres-solver.org/) (for calibration)
  - [Magick++](https://imagemagick.org/Magick++/) (for marker generation)

We have tested following combinations.

|OS|OpenCV|Eigen|OpenMP|Ceres-solver|Magick++|
|:--:|:--:|:--:|:--:|:--:|:--:|
| Ubuntu 18.04 |3.2.0|3.3.4|4.5| 1.14.0 | 6.9.7-4 |
| Mac OSX |4.1.0|3.3.7| - | 1.14.0 | - |

## How to use
If you have ImageMagick,
```bash
  cd build
  cmake ..
  make
  ./demoMarkerGenerationIM
  ./demoTRDM
```

### error related to pdf I/O
If you encounter an error saying **Magick: not authorized @ error/constitute.c/WriteImage/1037**, it's due to ImageMagick policy that disables postscript editing.  
To enable it, you can modify */etc/ImageMagick-6/policy.xml*  
from
```xml
<policy domain="coder" rights="none" pattern="PDF" />
```
to
```xml
<policy domain="coder" rights="read|write" pattern="PDF" />
```
so that ImageMagick can read and write pdf files.

If you don't have ImageMagick,
```bash
  cd build
  cmake ..
  make
  ./demoMarkerGeneration
  ./demoTRDM
```

### demoMarkerGeneration
This demo file generates random dot markers.  
The code generates a set of random dot markers and saves
- the pictures as *../data/markers%d.png*
- the marker information as *../data/markers.xml*

The xml file contains the markers information as follows.  
```xml
<?xml version="1.0"?>
<opencv_storage>
<num_markers>5</num_markers>
<image_width>2100</image_width>
<image_height>2970</image_height>
<num_points>50</num_points>
<markers>
  <_><_>
      2.7087266235640692e+02 2.4984686786722209e+03</_>
    <_>
      1.6149224434976149e+03 1.6742917104515509e+03</_>
    ...
    </_>
    ...
    </markers>
</opencv_storage>
```

You can change the settings by editing *data/gen_markers.xml*.  
Note that if *num_points* is too large, the code gets stacked in infinite loop!  
In such case, please decrease *num_points* or increase *image_width* and *image_height*.
- num_markers: The number of markers
- image_width: The image width
- image_height: The image height
- num_points: The number of dots per marker

### demoMarkerGenerationIM
This demo file also generates random dot markers using ImageMagick.
The difference between this and previous demos is (1) the image format (this demo saves the image as pdf) and (2) the unit of saved position (this demo saves in mm scale but the previous demo does in pixel scale).
The code generates a set of random dot markers and saves
- the pictures as *../data/markers%d.pdf*
- the marker information as *../data/markers.xml*  
<img src="markers0.png" width="400">

### demoTRDM
This demo file runs real-time tracking of markers as shown below.  
<img src="screenshot.png" width="400">

You can interact the code via keyboard as
- **s**: save the current image as *data/image%d.png* and the matching result as *data/matches%d.xml*
- **p**: turn on/off drawing matched points
- **m**: turn on/off drawing identified markers
- **q**: quit the demo

The matching result is saved as follows.  
```xml
<?xml version="1.0"?>
<opencv_storage>
<num_points>69</num_points>
<num_markers>5</num_markers>
<matches>
  <match0>
    <query_x>3.5519153594970703e+01</query_x>
    <query_y>4.2852755737304688e+02</query_y>
    <reference_x>2.6072961425781250e+02</reference_x>
    <reference_y>4.2387692260742188e+02</reference_y>
    <id_marker>2</id_marker>
    <id_point>3</id_point></match0>
  <match1>
    <query_x>2.9299627304077148e+01</query_x>
    <query_y>4.2727694702148438e+02</query_y>
    <reference_x>2.9426254272460938e+02</reference_x>
    <reference_y>2.1383207702636719e+02</reference_y>
    <id_marker>0</id_marker>
    <id_point>1</id_point></match1>
  ...
</matches>
</opencv_storage>
```

You can change the settings of LLAH, the tracking algorithm, by editing *data/build_llah.xml*.  
- n: The number of all NN points for considering nCm.
- m: The number of selected points for considering nCm.
- qbit: The number of bits for quantization

### demoCalibration
This demo runs camera calibration using TRDM.

Given a set of input images *data/image0.png* - *data/image8.png*, the code
1. load calibration configuration from *data/calibration_config.xml*
- load TRDM marker information from *data/markers.xml*
- setup TRDM tracking engine from *data/build_llah.xml*
- find corresponding points for each image
- load the found corresponding points
- run camera calibration
- save the camera parameters as *data/calibration_param.xml*
- visualize the reprojected points with the estimated parameters

In the xml file,
- node["Intrinsic"] stores the calibration matrix as cv::Mat
- node["Distortion"] stores the lens distortion parameter as cv::Mat
- node["NumOfExtrinsics"] stores the number as integer
- node["Extrinsics"] stores a set of extrinsic parameters
  - node["R"] of the node["Extrinsics"] stores a rotation vector as cv::Mat
  - node["t"] of the node["Extrinsics"] stores a translation vector as cv::Mat

```xml
<?xml version="1.0"?>
<opencv_storage>
<Intrinsic type_id="opencv-matrix">
  <rows>3</rows>
  <cols>3</cols>
  <dt>d</dt>
  <data>
    8.2484983675311616e+02 0. 3.2780079571614021e+02 0.
    8.2484983675311616e+02 2.4239332658927211e+02 0. 0. 1.</data></Intrinsic>
<Distortion type_id="opencv-matrix">
  <rows>8</rows>
  <cols>1</cols>
  <dt>d</dt>
  <data>
    -1.3461506743416838e-02 3.5623160189846924e-02
    2.2590901021922313e-03 3.1558162301614481e-03 0. 0. 0. 0.</data></Distortion>
<NumOfExtrinsics>13</NumOfExtrinsics>
<Extrinsics>
  <_>
    <R type_id="opencv-matrix">
      <rows>3</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        -3.2499270520353429e-01 -1.9348848382092290e-01
        -2.9293058926941553e+00</data></R>
    <t type_id="opencv-matrix">
      <rows>3</rows>
      <cols>1</cols>
      <dt>d</dt>
      <data>
        7.2120704640895610e+01 1.6397400660055598e+02
        3.6589762912462157e+02</data></t></_>
    ...
      </Extrinsics>
</opencv_storage>
```

#### How to change marker information
Change markers tag of *data/calibration_config.xml*.

#### How to change input images
Change images tag of *data/calibration_config.xml*.

#### How to change imaging model
Define a new struct in *Calibration.h* and *Calibration.cpp*.  
See Ceres-solver's [Tutorial](http://ceres-solver.org/nnls_tutorial.html#bundle-adjustment) and defined structs in the code.

## License of the code
This package is distributed under the GNU General Public License.
See the file [LICENSE](LICENSE) for further detail.

## Conditions of use
If you use this code for a publication, please cite the following paper:
```
@INPROCEEDINGS{TRDM,
    author = {Hideaki Uchiyama and Yuji Oyamada},
    title = {Transparent Random Dot Markers},
    booktitle = {International Conference on Pattern Recognition (ICPR)},
    year = {2018},
    pages = {254--259}
}
```

## Acknowledgements
The part of this implementation was supported by JSPS KAKENHI Grant Number JP16K16087.
