/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   demoCalibration.cpp
@brief  sample c++ program to use TRDM for camera calibration.
@author Yuji Oyamada
*/

#include <iostream>

#include "Markers.h"
#include "CalibrationUtil.h"


int main(int argc, char **argv)
{
    // step 0 load calibration configuration
    std::string filename_config = "../data/calibration_config.xml";
    std::string filename_markers;
    std::vector<std::string> filename_images;
    if(!load_calibration_config(filename_config, filename_markers, filename_images))
    {
        std::cerr << "calibration config file load failed..." << std::endl;
        return -1;
    };

    // step 1 load markers
    std::vector< std::vector< cv::Point2f > > markers;
    paramMarkers paramM;
    load_markers_from_xml(filename_markers, markers, paramM);
    markers.erase(markers.begin()+1, markers.end());
    std::cout << "    #markers: " << markers.size() << std::endl;
    std::cout << "    image_width: " << paramM.image_width << std::endl;
    std::cout << "    image_height: " << paramM.image_height << std::endl;
    std::cout << "    #points: " << markers[0].size() << std::endl;

    // step 2 build database
    std::cout << "START DATABSE CONSTRUCTION..." << std::endl;
    CLLAH db;
    if(!db.init("../data/build_llah.xml")){
        std::cerr << "llah config file load failed..." << std::endl;
        return -1;
    }
    db.build(markers);

    // step 3 find corresponding points
    std::cout << "START MARKER DETECTION..." << std::endl;
    std::string filename_data = "../data/calibration_data.xml";
    int th_min = 20;
    int th_max = 150;
    if(!save_corresponding_points(db, th_min, th_max, filename_images, filename_data)){
        std::cerr << "marker detection failed..." << std::endl;
        return -1;
    }

    // step 4 load corresponding points
    std::cout << "Load data..." << std::endl;
    filename_data = "../data/calibration_data.xml";
    std::vector<calibration::points3d> objects;
    std::vector<calibration::points2d> images;
    cv::Size size_image;
    if(!load_calibration_data(filename_data, objects, images, size_image))
    {
        std::cerr << "calibration data file load failed" << std::endl;
        return -1;
    }

    // step 5 run calibration
    std::cout << "START CALIBRATION..." << std::endl;
    calibration::cCalibration calibrator;
    bool flag_focal = true;     // set false if you want to change aspectratio
    bool flag_delta = false;    // set true if you want to use lens distortion parameters k3-k6
    calibrator.setData(size_image, objects, images);
    calibrator.initializeParameters();
    calibrator.optimizeParameters(flag_focal, flag_delta);
    calibrator.print();

    std::string filename_param = "../data/calibration_param.xml";
    calibrator.saveParam(filename_param);

    // step 6 draw reprojected points
    draw_results(calibrator.param, objects, images, filename_images);

    return 0;
}
