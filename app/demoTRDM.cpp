/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   demoTRDM.cpp
@brief  sample c++ program to play TRDM.
@author Yuji Oyamada
*/

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include "opencv2/opencv.hpp"

#include "Markers.h"
#include "LLAH.h"

bool load_config_llah(
    const std::string filename,
    sLLAHParam& param)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        return false;
    }

    param.n = int(fs["n"]);
    param.m = int(fs["m"]);
    param.k = 4;
    param.qbit = int(fs["qbit"]);

    fs.release();
    return true;
}

void test_LLAH()
{
    // step 1 load markers
    std::string filename_markers = "../data/markers.xml";
    std::vector< std::vector< cv::Point2f > > markers;
    paramMarkers param;
    load_markers_from_xml(filename_markers, markers, param);
    std::cout << "    #markers: " << param.num_markers << std::endl;
    std::cout << "    image_width: " << param.image_width << std::endl;
    std::cout << "    image_height: " << param.image_height << std::endl;
    std::cout << "    #points: " << param.num_points << std::endl;

    // step 2 build database
    std::string filename_llah = "../data/build_llah.xml";
    sLLAHParam paramLLAH;
    load_config_llah(filename_llah, paramLLAH);

    CLLAH db;
    db.init(paramLLAH);
    db.build(markers);

    // step 3 retrieve points
    for(int n = 0; n < markers.size(); ++n)
    {
        std::cout << "Marker[" << n << "]" << std::endl;
        std::vector<sRetrieval> matches;
        db.find(markers[n], matches);
        for(int m = 0; m < matches.size(); ++m)
        {
            std::cout << matches[m].id_marker << "." << matches[m].id_point << std::endl;
        }
    }

}

int th_max, th_min;

static void on_trackbar(int, void*){
    th_max = std::max(5, th_max);
}

std::vector< cv::Point2f > detect_dots(cv::Mat& img)
{
    cv::Mat img_edge;
    const int thlow = 30;		// lowest responce for edge
    const int thhigh = 100;		// edge if more than thhigh
    cv::Canny(img, img_edge, thlow, thhigh);

    std::vector< std::vector< cv::Point > > pt_contours;
    cv::findContours(img_edge, pt_contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

    std::vector< cv::Point2f > pt_dots;

    // circle size
    for(int n = 0; n < pt_contours.size(); ++n)
    {
        std::vector< cv::Point > &pt_contour = pt_contours[n];
        int size_contour = int(pt_contour.size());
        if(size_contour > th_min && size_contour < th_max)
        {
            cv::RotatedRect rrt = cv::fitEllipse(pt_contour);

            float ratio = rrt.size.width > rrt.size.height ? rrt.size.width / rrt.size.height : rrt.size.height / rrt.size.width;

            const float th = 3.0f;
            if (ratio < th){
                pt_dots.push_back(rrt.center);
            }
        }
    }
    return pt_dots;

}

void draw_points(
    cv::Mat& img,
    const std::vector<cv::Point2f>& pts
)
{
    for(int n = 0; n < pts.size(); ++n){
        cv::circle(img, pts[n], 5, cv::Scalar(255, 0, 0), -1);
    }
}

int main(int argc, char **argv)
{
    // step 1 load markers
    std::string filename_markers = "../data/markers.xml";
    std::vector< std::vector< cv::Point2f > > markers;
    paramMarkers param;
    load_markers_from_xml(filename_markers, markers, param);
    std::vector<cv::Point2f> pt_corners = {
        cv::Point2f(0, 0),
        cv::Point2f(param.image_width, 0),
        cv::Point2f(param.image_width, param.image_height),
        cv::Point2f(0, param.image_height)
    };

    std::cout << "    #markers: " << param.num_markers << std::endl;
    std::cout << "    image_width: " << param.image_width << std::endl;
    std::cout << "    image_height: " << param.image_height << std::endl;
    std::cout << "    #points: " << param.num_points << std::endl;

    // step 2 build database
    std::string filename_llah = "../data/build_llah.xml";
    sLLAHParam paramLLAH;
    load_config_llah(filename_llah, paramLLAH);

    CLLAH db;
    db.init(paramLLAH);
    db.build(markers);

    // step 3 setup camera
    cv::VideoCapture cap(0);
    if(!cap.isOpened())
    {
        std::cerr << "no camera detected!" << std::endl;
        return -1;
    }

    // step 4 retrieve points
    std::string window_name = "TRDM demo";
    cv::namedWindow(window_name);

    std::string trackbar_name = "threshold";
    th_min = 20;
    th_max = 50;
    cv::createTrackbar(trackbar_name, window_name, &th_max, 150, on_trackbar);
    on_trackbar(th_max, 0);

    cv::Mat _img, img;
    std::vector<sRetrieval> matches;

    bool flag_point = false;
    bool flag_marker = false;

    int id_save = 0;
    cv::Mat img_draw;
    while(cap.read(_img))
    {
        // detect dots
        cv::cvtColor(_img, img, cv::COLOR_BGR2GRAY);
        auto pt_queries = detect_dots(img);

        // find from database
        db.find(pt_queries, matches);

        // draw the results
        _img.copyTo(img_draw);
        draw_points(img_draw, pt_queries);
        if(flag_point)  db.draw_points(img_draw);
        if(flag_marker) db.draw_markers(img_draw, pt_corners);

        // interaction
        cv::imshow(window_name, img_draw);
        int key = cv::waitKey(1);
        if(key == 'q')  break;
        else if(key == 'p') flag_point = !flag_point;
        else if(key == 'm') flag_marker = !flag_marker;
        else if(key == 's'){
            cv::imwrite("../data/image" + std::to_string(id_save) + ".png", _img);
            db.save_matches("../data/matches" + std::to_string(id_save) + ".xml");
            id_save++;
        }
    }

    cv::destroyAllWindows();
    return 0;
}
